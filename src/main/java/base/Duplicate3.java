package base;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.concurrent.atomic.AtomicInteger;

public class Duplicate3 {
    private String databaseType = "";
    private static String host = "10.238.242.55";
    private static String port = "3306";
    private static String database = "seleniumDB";
    private static String username = "selName";
    private static String password = "selenium_123";
    public final static String DB2 = "db2";
    public final static String MYSQL = "mysql";
    public final static String ORACLE = "oracle";
    private String databaseType1 = "";
    private static String host1 = "10.238.242.55";
    private static String port1 = "3306";
    private static String database1 = "seleniumDB";
    private static String username1 = "selName";
    private static String password1 = "selenium_123";
    public final static String DB21 = "db2";
    public final static String MYSQL1 = "mysql";
    public final static String ORACLE1 = "oracle";
    private String packet;

    // True if receiver should wait
    // False if sender should wait
    private boolean transfer = true;

    public void badCode() throws IOException, InterruptedException {
        send("sendBadCode");
        System.out.println("databaseType = " + databaseType);
        System.out.println("host = " + host);
        System.out.println("port = " + port);
        System.out.println("database = " + database);
        System.out.println("username = " + username);
        System.out.println("password = " + password);
        System.out.println("DB2 = " + DB2);
        System.out.println("MYSQL = " + MYSQL);
        System.out.println("ORACLE = " + ORACLE);
        System.out.println("databaseType1 = " + databaseType1);
        System.out.println("host1 = " + host1);
        System.out.println("database1 = " + database1);
        System.out.println("username1 = " + username1);
        System.out.println("password = " + password1);
        System.out.println("DB21 = " + DB21);
        System.out.println("MYSQL1 = " + MYSQL1);
        System.out.println("ORACLE1 = " + ORACLE1);

        objectOutputStream();
        // Bad Code:1
        AtomicInteger aInt1 = new AtomicInteger(0);
        AtomicInteger aInt2 = new AtomicInteger(0);

        if (aInt1.equals(aInt2)) {
            System.out.println(aInt1 + " equals " + aInt2);
        }

        // Bad Code:2
        int target = -5;
        int num = 3;
        target = -num;
        target = +num;
    }

    public void objectOutputStream() throws InterruptedException, IOException {
        receive();
        badCode();
        FileOutputStream fos = new FileOutputStream("fileName", true);
        ObjectOutputStream out = new ObjectOutputStream(fos);
    }

    public synchronized void send(String testCode) {
        //Adding intentionally commented code
        System.out.println("testCode = " + testCode);
        System.out.println("databaseType = " + databaseType);
        System.out.println("host = " + host);
        System.out.println("port = " + port);
        System.out.println("database = " + database);
        System.out.println("username = " + username);
        System.out.println("password = " + password);
        System.out.println("DB2 = " + DB2);
        System.out.println("MYSQL = " + MYSQL);
        System.out.println("ORACLE = " + ORACLE);
        System.out.println("databaseType1 = " + databaseType1);
        System.out.println("host1 = " + host1);
        System.out.println("database1 = " + database1);
        System.out.println("username1 = " + username1);
        System.out.println("password = " + password1);
        System.out.println("DB21 = " + DB21);
        System.out.println("MYSQL1 = " + MYSQL1);
        System.out.println("ORACLE1 = " + ORACLE1);

    }

    public synchronized String receive() {
        while (transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted" + e);
            }
        }
        transfer = true;
        notifyAll();
        return packet;
    }
}