package base;

public class Singleton {
    // DON'T DO THIS!
    private String name;
    private static Singleton instance = null;

    Singleton(String name) {
        this.name = name;
    }

    public static Singleton getInstance() {
        if (instance == null) {
            instance = new Singleton("George");
        }
        return instance;
    }

    public String getName() {
        getInstance();
        return name;
    }
}
