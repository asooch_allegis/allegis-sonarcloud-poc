package base;

public class Singleton2 {
    // DON'T DO THIS!
        private String name;
        private static Singleton2 instance = null;

        private Singleton2(String name) {
            this.name = name;
        }

        public static Singleton2 getInstance() {
            if(instance == null) {
                instance = new Singleton2("George");
            }
            return instance;
        }

        public String getName() {
            return name;
        }
    }
