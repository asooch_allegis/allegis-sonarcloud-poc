package base;

import org.testng.annotations.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class Duplicate {
    private String databaseType = "";
    private static String host = "10.238.242.55";
    private static String port = "3306";
    private static String database = "seleniumDB";
    private static String username = "selName";
    private static String password = "selenium_123";
    public final static String DB2 = "db2";
    public final static String MYSQL = "mysql";
    public final static String ORACLE = "oracle";
    private String packet;

    // True if receiver should wait
    // False if sender should wait
    private boolean transfer = true;

    public Duplicate() throws IOException, InterruptedException {
        this.objectOutputStream();
        this.send("Testing");
        this.receive();
    }

    public void badCode() {
        // Bad Code:1
        AtomicInteger aInt1 = new AtomicInteger(0);
        AtomicInteger aInt2 = new AtomicInteger(0);

        if (aInt1.equals(aInt2)) {
            System.out.println(aInt1 + " equals " + aInt2);
        }

        // Bad Code:2
        int target = -5;
        int num = 3;
        target =- num;
        target =+ num;
    }

    public void objectOutputStream() throws InterruptedException, IOException {
        FileOutputStream fos = new FileOutputStream("fileName" , true);
        ObjectOutputStream out = new ObjectOutputStream(fos);
    }

    public synchronized void send(String packet) {
        while (!transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted" + e.toString());
            }
        }
        transfer = false;

        while (transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted" + e.toString());
            }
        }
        transfer = true;

        this.packet = packet;
        notifyAll();
    }

    public synchronized String receive() {
        while (transfer) {
            try {
                wait();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                System.out.println("Thread interrupted" + e);
            }
        }
        transfer = true;
        notifyAll();
        return packet;
    }
}