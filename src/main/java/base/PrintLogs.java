package base;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class PrintLogs {
    public String username = "username_test";
    public String password = "password_test";
    public String IP = "128.256.128.256";
    InetAddress startIP = InetAddress.getByName("101.101.101.101");

    public String username2 = "username_test";
    public String password2 = "password_test";
    public String IP2 = "128.256.128.256";
    InetAddress startIP2 = InetAddress.getByName("101.101.101.101");

    public PrintLogs(String testName) throws InterruptedException, UnknownHostException {
        System.out.println(">>>>>> " + testName);
        this.PrintLogsTest();
        this.doPrintLogs();
        this.PrintLogsTest2();
    }

    public void PrintLogsTest() throws InterruptedException {
        System.out.println("username: " + username);
        Thread.sleep(1000);
        System.out.println("password: " + password);
        Thread.sleep(1000);
        System.out.println("IP: " + IP);
        Thread.sleep(1000);
        System.out.println("startIP: " + startIP);
        Thread.sleep(1000);
    }

    public PrintLogs() throws UnknownHostException, InterruptedException {
        PrintLogsTest();
    }

    public void printLogs() {
        int i = 0;
        while (i < 10) {
            System.out.println("Printing logs" + i);
            i++;
        }
    }

    public void doPrintLogs() throws InterruptedException {
        Thread.sleep(1000);
        printLogs();
        Thread.sleep(1000);
    }

    public void PrintLogsTest2() throws InterruptedException {
        System.out.println("username: " + username);
        Thread.sleep(1000);
        System.out.println("password: " + password);
        Thread.sleep(1000);
        System.out.println("IP: " + IP);
        Thread.sleep(1000);
        System.out.println("startIP: " + startIP);
        Thread.sleep(1000);
    }
}