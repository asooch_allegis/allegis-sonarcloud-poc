package base;

import org.testng.annotations.Test;

public class SingletonTest2 {
    // DON'T DO THIS!
    private String name;
    private static SingletonTest2 instance = null;

    private SingletonTest2(String name) {
        this.name = name;
    }

    public static SingletonTest2 getInstance() {
        if (instance == null) {
            instance = new SingletonTest2("George");
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    @Test
    public void validateSingletonTest() {
        assert SingletonTest2.getInstance().getName() == "George";
    }
}