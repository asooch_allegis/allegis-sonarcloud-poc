package base;

import org.testng.annotations.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class PrintLogsTest {
    public String username = "username_test";
    public String password = "password_test";
    public String IP = "128.256.128.256";
    InetAddress startIP = InetAddress.getByName("101.101.101.101");

    @Test
    public PrintLogsTest() throws UnknownHostException {
        System.out.println("username: " + username);
        System.out.println("password: " + password);
        System.out.println("IP: " + IP);
        System.out.println("startIP: " + startIP);
    }

    @Test
    public void printCalender() throws Exception {
        PrintLogs pl = new PrintLogs();
        pl.printLogs();
    }

    @Test
    public void singletonTest() throws Exception {
        Singleton singleton = new Singleton("Singleton instance");
        singleton.getName();
        singleton.getInstance();
    }

    @Test
    public void dupeTest() throws Exception {
        PrintLogs logs = new PrintLogs("...SonarCloud testing...");
        logs.printLogs();
    }
}