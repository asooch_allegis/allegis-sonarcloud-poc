package base;

import org.testng.annotations.Test;

public class SingletonTest {
    // DON'T DO THIS!
    private String name;
    private static SingletonTest instance = null;

    private SingletonTest(String name) {
        this.name = name;
    }

    public static SingletonTest getInstance() {
        if (instance == null) {
            instance = new SingletonTest("George");
        }
        return instance;
    }

    public String getName() {
        return name;
    }

    @Test
    public void validateSingletonTest() {
        assert(SingletonTest.getInstance().getName() == "George");
    }
}