package base;

import org.openqa.selenium.Capabilities;
import org.testng.annotations.Test;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.concurrent.atomic.AtomicInteger;

public class DockerTest {
    @Test (enabled = false, invocationCount = 1, threadPoolSize = 1)
    public void DockerSeleniumTest() throws IOException {
        Capabilities chromeCapabilities = DesiredCapabilities.chrome();
        RemoteWebDriver chrome = new RemoteWebDriver(new URL("http://10.62.234.22:4444/wd/hub"), chromeCapabilities);

        chrome.get("https://www.allegisgroup.com");
        System.out.println(">>>>>>>> "+ chrome.getTitle() + " <<<<<<<<<<");
        chrome.quit();
            }
        }