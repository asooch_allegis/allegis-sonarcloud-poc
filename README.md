[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)
# Allegis Maven SonarCloud POC Project

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=asooch_allegis_allegis-sonarcloud-poc)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=bugs)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=code_smells)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=coverage)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=ncloc)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=alert_status)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=security_rating)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=sqale_index)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=asooch_allegis_allegis-sonarcloud-poc&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=asooch_allegis_allegis-sonarcloud-poc)

This project uses the [SonarScanner for Maven](https://redirect.sonarsource.com/doc/install-configure-scanner-maven.html) to trigger the analysis. Bitbucket Pipelines is configured to build and analyze all branches and pull requests.

See [this PR](https://bitbucket.org/sonarsource/sample-maven-project/pull-requests/1) as example.


# Run under Git Bash 
asooch@W10PERFTEST007 MINGW64 /u/opt/git/allegis-sonarcloud-poc (master)

# SonarQube 
mvn clean install sonar:sonar   -Dsonar.projectKey=sonarpoc   -Dsonar.host.url=http://rppd-devops01.allegisgroup.com:9000   -Dsonar.login=0455da0dcb319b0f1a52e2223ccc6a5ee940165c

# SonarCloud
mvn clean install sonar:sonar -Dsonar.projectKey=asooch_allegis_allegis-sonarcloud-poc -Dsonar.organization=asooch-allegis -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=71277ccf9f3dc7231e29dee9076add1dd568ed4f
